package epam.task.ionio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

public class NIOClient {
    private static final int PORT = 1111;
    private static final String HOST = "localhost";

    public static void main(String[] args) throws IOException, InterruptedException {

        InetSocketAddress moonAddr = new InetSocketAddress(HOST, PORT);
        SocketChannel moonClient = SocketChannel.open(moonAddr);

        log("Connecting to Server on port " + PORT);

        ArrayList<String> messagesToMoon = new ArrayList<String>();

        // create a ArrayList with mewssages
        messagesToMoon.add("Moon_night");
        messagesToMoon.add("Moon_day");
        messagesToMoon.add("Some_message");
        messagesToMoon.add("Good_night");
        messagesToMoon.add("River");

        for (String messageTo : messagesToMoon) {

            byte[] message = new String(messageTo).getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(message);
            moonClient.write(buffer);

            log("sending: " + messageTo);
            buffer.clear();

            // wait for 2 seconds before sending next message
            Thread.sleep(2000);
        }
        moonClient.close();
    }

    private static void log(String str) {
        System.out.println(str);
    }
}