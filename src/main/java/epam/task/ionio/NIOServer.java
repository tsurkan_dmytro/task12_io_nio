package epam.task.ionio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {

    private static final int PORT = 1111;
    private static final String HOST = "localhost";

    public static void main(String[] args) throws IOException {

    Selector selector = Selector.open();

    // ServerSocketChannel: selectable channel for stream-oriented listening sockets
    ServerSocketChannel moonSocket = ServerSocketChannel.open();
    InetSocketAddress mooonAddr = new InetSocketAddress(HOST, PORT);

    // Binds the channel's socket to listen for connections
		moonSocket.bind(mooonAddr);

    // Adjusts this channel's blocking mode.
		moonSocket.configureBlocking(false);

    int ops = moonSocket.validOps();
    SelectionKey selectKy = moonSocket.register(selector, ops, null);

    //server running
		while (true) {

        log("Waiting for new connection and buffer select...");
        // Selects a set of keys whose corresponding channels are ready for I/O operations
        selector.select();

        // token representing the registration of a SelectableChannel with a Selector
        Set<SelectionKey> moonKeys = selector.selectedKeys();
        Iterator<SelectionKey> moonIterator = moonKeys.iterator();

        while (moonIterator.hasNext()) {
            SelectionKey myKey = moonIterator.next();

            // if a new socket connection
            if (myKey.isAcceptable()) {
                SocketChannel moonClient = moonSocket.accept();

                // blocking to false
                moonClient.configureBlocking(false);

                // Read operations
                moonClient.register(selector, SelectionKey.OP_READ);
                log("Connection Accepted: " + moonClient.getLocalAddress() + "\n");

                // Tests whether this key's channel is ready for reading
            } else if (myKey.isReadable()) {

                SocketChannel crunchifyClient = (SocketChannel) myKey.channel();
                ByteBuffer crunchifyBuffer = ByteBuffer.allocate(256);
                crunchifyClient.read(crunchifyBuffer);
                String result = new String(crunchifyBuffer.array()).trim();

                log("Message received: " + result);

                if (result.equals("River")) {
                    crunchifyClient.close();
                    log("\nIt's time to close connection as we got last message 'Moon'");
                    log("\nServer will keep running. Try running client again to establish new connection");
                }
            }
            moonIterator.remove();
        }
    }
}

    private static void log(String str) {
        System.out.println(str);
    }
}
